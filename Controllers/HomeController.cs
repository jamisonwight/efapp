using Microsoft.AspNetCore.Mvc;

namespace angularCore.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
